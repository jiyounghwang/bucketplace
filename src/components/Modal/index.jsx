import React, { Component } from 'react'
import styled from 'styled-components'

export class Modal extends Component {
  overlayRef = null

  handleClick = (event) => {
    if (event.target === this.overlayRef) {
      this.props.onClose()
    }
  }

  render() {
    return (
      <Overlay innerRef={x => this.overlayRef = x} onClick={this.handleClick}>
        <Content>{this.props.children}</Content>
      </Overlay>
    )
  }
}

const Overlay = styled.div`
  top: 0;
  left: 0; 
  right: 0;
  bottom: 0;
  z-index: 1;
  position: absolute;
  background-color: rgba(0,0,0,0.5);
`

const Content = styled.div`
  top: 50%;
  left: 50%;
  width: 500px;
  height: 500px;
  position: absolute;
  background-color: #fff;
  transform: translateX(-50%) translateY(-50%);
`