import React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'

export const Menu = () => (
  <div>
    <Wrapper><Link to='/'>전체 보기</Link></Wrapper>
    <Wrapper><Link to='/book-mark'>북마크</Link></Wrapper>
    <hr/>
  </div>
)

const Wrapper = styled.span`
  margin-right: 16px;
`