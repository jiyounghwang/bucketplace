import React, { Component } from 'react'
import styled from 'styled-components'
import { Modal } from '../Modal'
import { BookmarkIcon } from '../BookmarkIcon'

export class ProductCard extends Component {
  state = { isModalOpen: false, isBookmarked: false }

  openModal = () => {
    const body = document.querySelector('body')
    body.style.position = 'fixed'
    body.style.overflow = 'hidden'
    body.style.height = '100%'
    this.setState({isModalOpen: true})
  }

  closeModal = () => {
    const body = document.querySelector('body')
    body.style.position = 'relative'
    body.style.overflow = 'visible'
    this.setState({isModalOpen: false})
  }

  handleBookmark = () => {
    this.setState({ isBookmarked: !this.state.isBookmarked })
  }

  render() {
    const { product } = this.props
    const { isBookmarked, isModalOpen } = this.state

    return (
      <div>
        {product.image_url && <Img src={product.image_url} onClick={this.openModal}/>}
        <div style={{position: 'relative'}}>
          <Wrapper>
            <Category>{product.type}</Category>
            <BookmarkButton onClick={() => this.handleBookmark(product.id)}>
              <BookmarkIcon isBookmarked={isBookmarked}/>
            </BookmarkButton>
          </Wrapper>
        </div>
        {isModalOpen && <Modal onClose={this.closeModal}><ModalImg src={product.image_url}/></Modal>}
      </div>
    ) 
  }
} 

const Img = styled.img`
  width: 100%;
  height: 200px;
  cursor: pointer;
  object-fit: cover;
`

const ModalImg = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`

const BookmarkButton = styled.button`
  appearance: none;
  cursor: pointer;
  border: none;
  background: none;
  &:focus {
    outline: none;
  }
`

const Wrapper = styled.div` 
  position: absolute;
  bottom: 8px;
  display: flex;
  justify-content: space-between;
  width: 100%;
`

const Category = styled.span`
  background-color: rgba(0,0,0,0.6);
  color: #fff;
  padding: 8px;
  margin: 8px;
  border-radius: 4px;
`