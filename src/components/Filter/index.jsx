import React from 'react'
import styled from 'styled-components'

export const Filter = ({ onSelectCategory }) => (
  <div>
    <Anchor onClick={() => onSelectCategory('all')}>모두 보기</Anchor>
    <Anchor onClick={() => onSelectCategory('Card')}>사진</Anchor>
    <Anchor onClick={() => onSelectCategory('Production')}>제품</Anchor>
    <Anchor onClick={() => onSelectCategory('Project')}>집들이</Anchor>
    <Anchor onClick={() => onSelectCategory('Exhibition')}>기획전</Anchor>
    <hr/>
  </div>
)

const Anchor = styled.a`
  margin-right: 16px;
`