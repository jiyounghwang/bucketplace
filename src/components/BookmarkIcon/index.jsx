import React from 'react'

export const BookmarkIcon = ({ isBookmarked }) => (
  isBookmarked ? 
    <img src="https://s3.ap-northeast-2.amazonaws.com/bucketplace-coding-test/res/action-scrap-circle-b.svg" alt="bookmark"/> 
      : 
    <img src="https://s3.ap-northeast-2.amazonaws.com/bucketplace-coding-test/res/action-scrap-circle-w.svg" alt="bookmark"/>
)

