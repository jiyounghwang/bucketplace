import React from 'react'
import Waypoint from 'react-waypoint'
import styled from 'styled-components'
import { ProductCard } from '../ProductCard'

export const ProductList = ({ products, loadMoreProducts }) => {
  return (
    <Grid>
      {products.map((product, index) => (
        <ProductCard product={product} key={index}/>
      ))}
      <Waypoint onEnter={loadMoreProducts}/>
    </Grid>
  )
}

const Grid = styled.div`
  display: grid;
  grid-column-gap: 1em;
  grid-template-columns: repeat(4, 1fr);
`