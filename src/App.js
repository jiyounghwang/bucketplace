import React, { Component } from 'react';
import styled from 'styled-components'
import { Route } from 'react-router-dom';
import { Home, Bookmark } from './pages';
import { Menu } from './components/Menu'

class App extends Component {
  render() {
    return (
      <Wrapper>
        <Menu/>
        <Route exact path="/" component={Home}/>
        <Route path="/book-mark" component={Bookmark}/>
      </Wrapper>
    );
  }
}

const Wrapper = styled.div`
  padding: 80px;
`

export default App