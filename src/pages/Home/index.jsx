import React, { Component } from 'react';
import axios from 'axios'
import { Filter } from '../../components/Filter'
import { ProductList } from '../../components/ProductList'

export class Home extends Component {
  state = { products: [], category: 'all', pagination: 1, isLoading: true }

  componentDidMount() {
    axios.get(`https://s3.ap-northeast-2.amazonaws.com/bucketplace-coding-test/feed/page_${1}.json`)
      .then(res => {
        const products = res.data
        this.setState({ products })
      })
  }

  handleChangeCategory = (category) => {
    this.setState({ category })
  }

  handleLoadMore = () => {
    this.setState((prevState) => ({
      pagination: prevState.pagination + 1
    }))
    console.log(this.state.pagination)
    // TODO: infinite scroll
  }

  render() {
    const { products, category } = this.state
    const filteredProducts = category === 'all' ? products : products.filter(x => x.type === category)

    return (
      <div>
        <Filter onSelectCategory={this.handleChangeCategory} />
        <ProductList 
          products={filteredProducts} 
          loadMoreProducts={this.handleLoadMore} 
        />
      </div>
    ) 
  }
}